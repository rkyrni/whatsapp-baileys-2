const {
  DisconnectReason,
  useSingleFileAuthState,
} = require("@adiwajshing/baileys");
const makeWASocket = require("@adiwajshing/baileys").default;
const { ChatAIHandler } = require("./feature/chat_ai.js");

const startSock = () => {
  const { state, saveState } = useSingleFileAuthState("./auth.json");
  const sock = makeWASocket({
    printQRInTerminal: true,
    auth: state,
  });

  sock.ev.on("connection.update", function (update, connection2) {
    let _a, _b;
    const { connection, lastDisconnect } = update;
    if (connection === "close") {
      if (
        ((_b =
          (_a = lastDisconnect.error) === null || _a === void 0
            ? void 0
            : _a.output) === null || _b === void 0
          ? void 0
          : _b.statusCode) !== DisconnectReason.loggedOut
      ) {
        startSock();
      }
    } else {
      console.log("connection closed");
    }

    console.log("connection update", update);
  });

  sock.ev.on("creds.update", saveState);

  sock.ev.on("messages.upsert", async (m) => {
    const msg = m.messages[0];

    console.log("hahaha", msg.message);

    if (!msg.key.fromMe && m.type === "notify") {
      if (msg.message) {
        const msgClient = msg.message.conversation.toLowerCase();
        if (msgClient.includes("ask/")) {
          ChatAIHandler(msg.message.conversation, sock, msg.key.remoteJid);
        } else if (msg.message.buttonsResponseMessage) {
          await sock.sendMessage(msg.key.remoteJid, {
            text: "⭐ *https://tinyurl.com/2zorvjwb*",
          });
        } else {
          // await sock.sendMessage(msg.key.remoteJid, {
          //   text: "Hay, Saya bot buatan RR\n\nFormat yang kamu butuhkan:\n\n*-Bot Ai*\n*ask/ pertanyaan kamu*\ncontoh: ask/ berapa 17 x 4\n\n*-Pengubah Background*\n*edit_bg/warna*\ncontoh: edit_bg/red\nnote: tulis text format saat mengirim foto yang ingin di edit, warna juga dapat diberi dengan hexa warna seperti #FF34FF\n\nTerimakasih",
          // });

          // await sock.sendMessage(msg.key.remoteJid, {
          //   text: "⭐ *https://tinyurl.com/2zorvjwb*",
          // });
          const buttons = [
            {
              buttonId: "id1",
              buttonText: { displayText: "Follow Me ↗️" },
              type: 1,
            },
          ];

          const buttonMessage = {
            text: `Hay ${msg.pushName}, Saya bot buatan RR\n\nFormat yang kamu butuhkan:\n\n*-Bot Ai*\n*ask/ pertanyaan kamu*\ncontoh: ask/ berapa 17 x 4\n\n*-Pengubah Background*\n*edit_bg/warna*\ncontoh: edit_bg/red\nnote: tulis text format saat mengirim foto yang ingin di edit, warna juga dapat diberi dengan hexa warna seperti #FF34FF\n\nTerimakasih`,
            footer: "Tetapla hidup walaupun ga guna",
            buttons: buttons,
            headerType: 1,
          };

          await sock.sendMessage(msg.key.remoteJid, buttonMessage);
        }
      }
    }
  });
};

startSock();
